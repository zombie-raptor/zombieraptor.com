## Genres

Games consist of different "genres". The term "genre" in video games
is overloaded because it can refer to the mechanics or to the theme.
Here, "genre" means a mechanical genre, rather than the theme of the
game. Mechanical genres share certain conventions for controls, user
interfaces, and so on.

The Zombie Raptor game engine will have different presents for
different popular genres.

**Note: This is a design document. The goal is to go into extreme,
boring detail about the kinds of things that most gamers take for
granted when they play video games. Everything here should eventually
be implemented, but not everything is currently present in the current
built of the Zombie Raptor game engine. You do not need to read this
document in order to understand the APIs.**

### First person shooter (FPS)

Games of the **first person shooter** (FPS) genre will use first
person controls, described in detail in the [controls](controls.md)
design document.

First person shooters have three types of "weapons":

- Ranged weapons or guns. These use projectiles or hitscan that damage
  at a far distance. These might not be guns. They could be magic
  wands, laser weapons, rocket launchers, bows, etc.

- Melee weapons are things like swords, axes, chainsaws, fists, etc.
  These have limited range, but don't typically consume ammunition.

- Tools are things that take up a weapon slot and have similar
  controls to the weapons, but have some other action on the world.
  They could be used to build things, repair things, activate things,
  etc.

Games that are mechanically like first person shooters may behave
almost identically except for having a non-typical mix of the above.
For instance, entirely non-violent games might only have tools, if
anything. Alternatively, there might be a medieval game where melee
weapons are common and diverse, while there are only a handful of bows
available.

#### Health and damage

Players, AI-controlled enemies/allies, and objects in the world have
health, usually represented as a single hitpoint (HP) number, although
sometimes modeled in a more complicated way. They can also have armor
or shields, which exist to reduce incoming damage to the HP until
depleted.

Usually the "shield" terminology refers to a science fiction shield
that is recharging, while armor does not recharge. The earliest FPSes
often had multiple tiers of armor that had varying levels of damage
absorption, e.g. 2X and 3X.

Both HP and armor/shields may or may not automatically regenerate.
Regeneration can also be controlled by skills, or only happen under a
certain amount, etc. Health that does not regenerate might depend
instead on a medic ability, health pickups, etc., or might even
permanently be lost until the end of the round.

Characters and objects tend to have a hitbox, which is an invisible
box or sphere that roughly corresponds to the more detailed, visible
3D model. Damage taken tends to vary based on the location, with "head
shot" damage typically higher than normal damage and "leg shot" damage
typically lower, with torso damage as the expected damage. The degree
to which this matters usually depends on the weapon dealing the damage
rather than the character or object receiving the damage.

When the HP is 0 or negative, the character or object is killed or
destroyed. Some objects, such as explosive barrels, do damage to the
surroundings when this happens.

Sometimes the health of enemies are indicated. This can be above or on
top of an enemy; alternatively, it can be at the top of the screen.
The latter is usually reserved for bosses. Sometimes this health
indication is reserved for a particular inventory, skill, class, etc.
For balance reasons, it makes more sense with PvE than PvP. The health
indicator could also be restricted to only showing players on the same
team. If the latter is true, then the player name is probably also
useful to display. Health can be indicated exactly, with a number
and/or a progress bar. It can also be indicated in a fuzzy way, such
as green for high health and red for low health.

The hitbox is also used for determining collision with other objects
or with the environment.

#### UI

Graphically, the most notable distinguishing aspect of a first person
shooter is the weapon or item held in the player's hand(s) that takes
up a large portion of the foreground.

These weapons or items tend to have two separate models:

- A detailed model that's for the close-up first person view. This
  almost acts like part of the HUD.

- A lower resolution world model that is seen when held by someone
  else (human or AI), or when just on the ground.

The primary UI of the FPS is the first person HUD.

When a shot hits, some games play a hit sound, with a different pitch
depending on where it is hit. Usually, a head shot is the highest
pitch, but sometimes it's the other way around.

A hit might also have floating numbers to indicate the damage done
and/or points scored, especially if it's a very casual or arcade style
game.

#### First person HUD

The first person "head-up display" or "heads-up display" (HUD) is
usually minimal when contrasted with the UI of strategy games, with
most of the FPS HUD's information on the edges of the screen and most
of the remaining information near the center. Information near the
center is either around the crosshair or part of the crosshair.

The information displayed in the center can vary. It can be as simple
as the crosshair changing color to indicate one's health, or there
could be health and ammo indicators around the crosshair, or even more
complicated information can be displayed in the center instead of at
the periphery.

Most if not all of the HUD's information can be communicated in the
game world itself instead of via the HUD. This type of interface is
typically only done in VR games, where the traditional concept of a
HUD doesn't really work and where the additional immersion can be very
desirable.

When information isn't displayed as part of a HUD, it is usually
displayed as part of the currently active weapon, but could also be
displayed elsewhere, such as via a watch on the character's wrist.

Most games indicate the health, armor/shields, etc., on the HUD as
well as the ammo, if there is ammo. There usually are two ammo
numbers: the amount of ammo needed before reloading and the amount of
remaining ammo. Sometimes, if there's no reloading on a particular
weapon, only the latter is shown, even though the game normally has
the two displayed.

Some games, especially very old FPSes or games referencing those
FPSes, have the face of the player character on the HUD, which changes
based on the current health status. Other, more modern, FPSes using
similar concepts might display the entirety of the 3D player model or
some abstract outline representing the player's status. No matter
what, this sort of display will probably require a 3D-capable HUD
rather than the traditional sprites that used to serve this role.

The HUD also contains other important information, including:

- timers for objectives or respawns
- objective markers, overlaid as 2D icons on top of the 3D environment
- the player's rank
- points/score
- the current round/wave
- how many teammates and/or enemies are alive
- how many players are on each team
- a silhouette of the teammates (or even of the enemies!) that are
  behind walls
- active status effects (both positive and negative)

#### Spawning and respawning

Players spawn. That is, they are placed on the map with a "fresh"
start in predetermined location(s), to varying degrees depending on
how punishing death is in the game. If spawns are selectable, they
either spawn algorithmically or can select which one to use, perhaps
with a map. Sometimes, spawns can be constructed (and destroyed).

In multiplayer, spawning is usually a key mechanic. In single player,
spawning is usually only done at the start of a new map or level.
Inventory tends to persist between level starts if it's a campaign,
like in single player. In single player, one usually just goes back to
a previous save on death rather than respawning. Saving can be
automatic, manual, or both. Games that have manual saves often
distinguish between quicksaving and regular saves.

Inventory can be lost or kept on death. Lost inventory can be dropped
as loot or removed permanently or some mix of the two (i.e. partial
losses). Often, dropped weapons only give one clip's worth of
ammunition, especially in fast paced FPSes.

#### Classes

There are three ways to start in multiplayer:

- A class, which determines the starting weapons/skills/items, and
  perhaps even which weapons are available to use.
- A predetermined start, essentially just like everyone starting with
  one class.
- Buying.

What the player starts with when starting fresh is determined by their
class. Classes always exist, but there might only be one class. If
some other mechanism, such as weapon buying, is used, then the player
can start with "nothing", which might be a knife or just their fists.
If weapon buying is a key element, then perhaps "classes" are best
thought of as selectable loadouts that the player can save, which
saves time at the start of each round/match/game. Some reasonable
defaults would probably still be helpful.

Classes can be used to determine variations in traits like health and
speed and even the presence/absence of a double jump. This can be
accomplished with hidden, undroppable inventory items that persist
throughout one's life.

Classes might also restrict which weapons and abilities one is able to
use. This is especially the case in "hero" based games, which are
essentially just classes where each class has more of a personality
and might be limited to one per team. Classes might still have a cap
even if they let more than one player play that class, to prevent 100%
of people on a team choosing that class.

#### Death

Death can mean a lot of different things.

In single player, it could mean going back to the latest save or even
getting a game over, deleting the current save.

In multiplayer, it could mean respawning. This could be done either
instantly or on a respawn timer, with or without spawn protection
(temporary invincibility), and with the potential for different teams
to have different times, perhaps even dynamically determined by the
current situation.

In multiplayer, lives can be finite, including just having one. In
single player, lives are unlikely to be present, but some of the
earliest FPSes had them. With finite lives, you might be able to earn
more.

In the event of running out of lives, various actions could happen,
such as joining another team (such as the environmental enemies in PvE
or, more likely, just the observers, either observing globally or just
your old team). With matchmaking and finite lives, quickly joining
another server might be desirable, but it shouldn't be forced.

#### Statistics, points, and so on

The more things that are tracked, the more the player will want to
improve at a skill-based game, like most FPSes are.

Accuracy, kills, deaths, etc., can be tracked.

The `tab` menu should rank players on each team (or overall if
free-for-all or PvE). Points/score depend on the particular details of
a game, but should be accomplished by doing a role, such as healing,
well. Old games usually ranked primarily on kills, and that made
support classes unappealing. With ranking based on points instead,
balance is simply determining the rewards for doing a certain role,
not just fragging. Sometimes, someone can do more than one role within
one game, too, either by switching classes or by not having true
classes.

The most common way to get points is probably dealing damage and this
should generally be more rewarding than the support actions, but not
by much. Damage should be emphasized instead of kills because in
FPSes, one can "killsteal" by getting credit for the kill without
doing most of the damage. To compensate, some games give "assists",
which could be given to the player who did most of the damage, or
could even be given to a support role that supported the player who
got the kill.

Both kills and assists are often displayed by the game itself, usually
in the upper right of the screen.

#### Weapon buying

Certain kinds of FPses might prefer to use purchases to acquire
weapons, or even traits/inventory.

Menus should be entirely keyboard driven for fast use, including
saving potential preset loadouts/etc. for even faster action.

If holding down LMB/fire while in the menu, firing could persist to
allow someone to purchase without disrupting the gameplay at the cost
of less efficient ammo usage (due to the lack of aiming).

For an even more streamlined experience, ammo for the currently active
weapon could be bought in an even faster way, such as with the `b`
key.

By default, if using the concept of cash for buying, then cash should
be points, with the distinction that cash can be spent but points
don't go down while shopping. Friendly fire damage and/or teamkilling
might separately reduce one's points, though. If events deduct points,
removing cash for that as well is probably unfair.

Buying might be allowed at any time, or it could be restricted by
place and/or by a certain time in a round.

#### Multiplayer

Multiplayer typically adds a few features. This includes voice chat
(often `v`) and text chat (often `t` or even a combination of `t`,
`y`, and `u` depending on who the chat is aimed at). Voice chat is
usually restricted to one's team, but might be global or proximity.
Team chat might also leak into proximity chat if an enemy is nearby.

In multiplayer, there are one or more teams, or there is a free for
all with no teams. There are two general situations in games:
player-vs-player (PvP) or player-vs-environment (PvE). The latter is
always the case for single player (where the player fights the AI),
but the former is the common type of game for multiplayer.

With player vs player, the players either can fight each other or are
supposed to fight each other. With player vs environment, the main
opponent is enemies, typically some swarm-based enemy like zombies or
robots.

For multiplayer, one common setting is friendly fire. If it is on and
there are teams, someone can directly damage other players on their
team. Games with small, more serious teams usually allow this, while
more casual games with larger team sizes usually do not. With friendly
fire on, it's easy to "grief", i.e. when a player sabotages their own
team. Even with friendly fire off, team killing/damage can happen. For
instance, launching a player with an explosion that does no damage
because friendly fire is off still might give fall damage to that
player when they land.

##### Rounds

In multiplayer, one or more rounds could happen on the same map. If
there's only one round on the map, then the map switches at the end.
It might just switch to itself if the game itself only has one map!

Rounds can be timed, but what running out of the timer means could
vary. It might just be the player or team with the highest score at
the end wins, or it could mean that the "defender" wins or that
there's a draw (nobody wins). There are a lot of possibilities and
this should be scriptable.

There could be multiple timers per round, such as waves in a
wave-based survival game. If horror-themed, the wave timer could be
hidden from the player and might even be dynamic.

Objectives can exist, such as capture the flag, reaching a certain
location, etc. This is beyond the scope of this document for now.

##### Servers

There is a list of servers. Hosted game servers will, by default, talk
to a main server ("metaserver"?) that broadcasts this list so that
servers can appear on this. Players can sort by ping, player count,
map, game type, etc.

Players can also be automatically matched based on skill or ping to
servers, either run officially or hosted third party, or both.
