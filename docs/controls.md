## Controls

Different controls can exist based on the (mechanical) genre of the
game. The Zombie Raptor game engine aims to cover some of the most
common types of controls as built-in defaults that can be inherited
from.

**Note: This is a design document. The goal is to go into extreme,
boring detail about the kinds of things that most gamers take for
granted when they play video games. Everything here should eventually
be implemented, but not everything is currently present in the current
built of the Zombie Raptor game engine. You do not need to read this
document in order to understand the APIs.**

### First person controls

One of the most popular kinds of controls for 3D video games are first
person controls. They were first popularized in the first person
shooter (FPS) genre, but because of their popularity, the same
controls are used across many different types of games. These other
genres include most 3D puzzle games, "walking simulators", and so on.

This is perhaps the most popular and simplest control scheme for 3D
games so this is one of the highest priority of controls for Zombie
Raptor to support.

Close (over the shoulder) third person controls with player controlled
camera rotations are extremely similar to these controls, but with the
controllable character visible on the screen. Games might even allow a
toggle between first person and third person views.

However, third person cameras are harder to get right because having a
first person camera simplifies several considerations that a third
person camera has to deal with:

- You don't need to figure out how close the camera is.
- You don't need to determine if the camera collides with the
  environment, and if it does if it should block the rotation or zoom.
- You don't need to worry about bad camera angles making the game
  harder, which many early 3D games had to deal with.
- You don't have to worry about players getting an unfair advantage by
  be able to see things that they shouldn't be able to see while their
  player model is staring at a wall.

Instead, with first person controls, you effectively *are* the camera.
The camera rotates to look where you are looking and if the camera
collides with the environment it's because *you* are colliding with
the environment. This means that first person cameras are typically
one of the easier cameras to implement in a game engine.

There are some disadvantages first person:

- Vehicles are harder to control. In fact, many otherwise first person
  games have third person vehicles.
- Platforming (jumping from platform to platform) is typically much
  more difficult to judge.
- The appearance of the player character is typically not visible to
  the player (but it could be on the HUD).

First person games tend to be real-time, action-oriented, and with
incredibly consistent control schemes, especially on PC, where the
current control scheme has remained essentially unchanged since the
late 1990s. Even when other platforms have their own first person
controls, they're usually very uniform between games. This means that
the game engine defaults can largely be trusted here without much
customization for each individual game.

As mentioned before, first person games tend to be gun-based "first
person shooters" with guns and things with very similar controls to
the guns behaving as weapons. Still, plenty of non-FPS games using
first person controls have tools that behave extremely similar to FPS
guns because of the assumed familiarity a gamer has with the first
person control schemes.

First person controls usually don't emphasize the UI and menus,
further making it a high priority for early support inside of the
Zombie Raptor game engine. The main UI focus tends to be a minimal
"head-up-display", or "HUD". Other key UI elements tend to be menus
designed for keyboard navigation for quick action (such as a class or
weapon selection screen) as well as a developer/debug/cheat console,
which is, again, primarily designed for text display and keyboard
interaction.

First person controls in the ZR game engine will use the keyboard and
mouse for now because support for VR and non-PC platforms is not a
priority for the initial release. Controller use on PC is rare. Both
VR controllers and regular console-style gamepad controllers have
their own standardized first person control schemes that people expect
when playing such games. Note that aiming with a gamepad is harder
than with a mouse, so console-style controls often include aim assist
to help console players.

With the exception of VR, first person controls are typically defined
around always aiming at the center of the screen, which is usually
marked by a crosshair. That is, both aiming and looking are controlled
the same way. You aim in the direction that you face.

VR is an exception to this rule. If there is a crosshair present in
VR, it exists as part of the gun model, which effectively forces the
"aiming down sights" style of FPS aiming. In VR, aiming is done where
the gun or tool is facing rather than automatically going where the
center of the screen is. The VR player can look around without
changing where they are aiming. The game engine needs to keep this in
mind when designing first person aiming so that adding VR at a later
point is not too difficult.

No matter what, determining the closest object in the direction of
where the "crosshair" is facing is a key concept for virtually all
first person games. It would be hard to interact with the game world
without something like this, even if the game is a "walking simulator"
that does not involve any action or violence.

A key part of the camera is determined by the field of view (FOV),
which should be configurable by a slider to go within a certain range.
The range that is reasonable and what the number even means depends on
the matrices used to determine the first person camera. In general, a
low FOV will make the camera looked zoomed in, and in fact, the
zooming of certain scopes could use a FOV multiplier to achieve this
affect.

#### Core movement and interaction controls

The most core of the default modern first person control scheme is the
`WASD` or `WSAD` control scheme. That is, `W` moves the camera (and
character) forward and `S` backward. The other two keys provide
'strafing', which moves the character without rotating to that
direction. (In retro first person games, left and right would rotate.)
`A` strafes left and `D` strafes right. Note that the controls can be
independent of the keyboard layout with SDL2 scancodes. This assumes a
`QWERTY` keyboard, but the actual positions should be constant.

Usually, moving backwards or strafing is slower than moving forward.
Additionally, the movement is usually normalized to cap the speed and
prevent "strafe running", where diagonal movement is actually faster
than simply moving forward.

The other main component is the "mouse look" component. Looking up and
down is done with `mouseup` and `mousedown`, while rotating the
character/camera left and right is done with `mouseleft` and
`mouseright`. These two actions are technically unrelated, but most PC
gamers will be incredibly used to this.

Which direction `mouseup`/`mousedown` and `mouseleft`/`mouseright`
actually rotate the camera depends on configuration. A certain
direction was settled on, but the opposite can make just as much
sense, for either axis. This means that both x and y can, separately,
have "inverted" controls if the player chooses to configure them that
way. Not providing these options will disadvantage certain players who
are used to an unusual control scheme. By default, the positive y
looks up and the positive x moves to the right.

Another consideration for the mouse is the sensitivity. Technically,
the x axis and the y axis of mouse movement do independent activities
so each sensitivity could be configured separately, but this is
uncommon in games. Instead, the x axis sensitivity is generally some
multiplier of the y axis sensitivity that feels right.

In these sorts of games, `spacebar` is usually jump, if jumping is
permitted at all. There might be double jumps or even triple jumps,
achieved by multiple presses of the `spacebar`. Some engines even
support "crouch jumping", which is crouching right after jumping for a
bit of an extra height in one's jump. Defying realism, one is normally
able to control one's character while in their air while jumping, not
unlike a 2D platformer.

Game engines are generally split over whether the `use` key (for most
miscellaneous interactions) uses the `e` or `f` key. If the `e` key is
used for the interaction/use, then the `f` key is often available for
toggling a flashlight. For this reason, ZR will use `e` for `use` and
`f` for `flashlight` by default.

Technically, reloading is an action that's seen only in first person
shooters, but because those are so popular, it's worth mentioning here
in the core controls that `reload` is almost always `r`. If the first
person game does not having reloading, then some action still might be
bound as a 'reload' action, taking its place on the key/button
bindings.

The three mouse buttons usually represent primary (left), secondary
(right), and tertiary (middle; i.e. clicking the scroll wheel) actions
with the currently active/visible item or weapon. Other mouse buttons
are usually left available for the player to customize since not every
mouse has those buttons.

If aiming down sights (ADS) is a key part of the game and/or weapon,
then that usually takes the secondary mouse button, but might take the
tertiary. It might be a toggle or it might need to be held. ZR will
default to holding, but will permit toggling. If the zoom has some
kind of sniper rifle scope or represents binoculars, then the zoom is
often configurable with the mouse scroll wheel, which would otherwise
represent weapon/item switching outside of this mode.

Without ADS, the secondary mouse button is probably the "alt fire".
Without ADS, the "alt fire" is probably done with the tertiary mouse
button. Rarely, there might be a key or mouse button that switches the
mode rather than a proper "alt fire". This mode switch might be the
secondary mouse button or it could be essentially any key on the
keyboard since there is no standard here.

There are two forms of aiming down sights (ADS). The more common one
actually has weapons with sights/scopes that are used. The weapon
model is centered (instead of being on the left or right side,
typically right) and raised and the sights are used for aiming instead
of (or in addition to) the screen's crosshair. A fast paced,
retro-style FPS (or a non-FPS) might instead use the same concept for
a "zoom" rather than actually having sights. In this case, the ADS
button (usually the right mouse button) will just increase the zoom
(probably by decreasing the FOV) when held. This zoom might also be
its own key on the keyboard, such as `z`.

Proper aiming down sights usually reduces the mouse sensitivity to
allow for finer aiming and to compensate for the fact that the view
has been zoomed. A good implementation of non-toggled aiming down
sights should also allow reloading while holding the right mouse
button, resuming the ADS as soon as the reloading animation is
finished.

#### Modifier controls

Generally, there are several modifier keys. These might be held or
they might be toggled. ZR will default to holding, but will also
support toggling.

`shift` is usually `sprint`. If the game defaults to always running,
then `shift` might be `ealk` instead. Often, `shift` temporarily
modifies this state and `capslock` toggles this state, reversing which
behavior is default. Occasionally, there are three states at different
speeds with the highest speed stamina-limited: walk, run, and sprint.

Walking is often seen as unnecessary, especially if crouching moves at
effectively the same reduced speed, but with an accuracy bonus.

Crouch is usually `ctrl` or `c`. ZR will use `ctrl` because of
personal preference of the developer.

Sometimes, another modifier is toggling the camera, usually to one or
more close third person camera angles with essentially the same
controls. There might be two, since the player model might be on the
left or the right instead of obstructing a view of the center.

#### Weapons and items

Weapons or items/tools that behave like FPS weapons tend to be stored
in "weapon slots". These slots are usually `1` through `9`, but might
include `0`.

Weapons are present or absent in slots. Usually more than one weapon
can go into a slot, which can be cycled with repeated pressing of that
particular number.

Cycling through all of the weapons can be done with the mouse wheel
up/down. Whether up goes up in slots or down is essentially arbitrary.

Generally, weapons/etc. go into a predetermined slot. They also
usually go into a predetermined order of that slot if more than one go
to one slot, but they might also be in order of pickup. If an
RPG-style first person game, then there might only be one weapon/item
per slot, with the player determining the exact placement based on the
inventory screen.

Some games use `q` to quick switch to the previously used slot. This
is rare, but extremely useful and so it will be supported.

"Throwable" items might have `g` (for "grenade") as a hotkey for quick
use. They still usually have their own weapon/item slot, even in games
with this key.

Controllers (VR or gamepad) typically use a weapon wheel for weapon
selection. Some games preserve this weapon wheel even when using the
keyboard/mouse controls.

Usually, the "weapons" are sorted by power or type. For instance,
primary could be `1`, secondary `2`, and melee `3` or the other way
around. This probably makes sense if there is a weapon cap or if it is
a class-based game with fixed starting weapons.

Alternatively, if it's a game with no cap, then perhaps `2` is all of
the pistols, `3` is all of the shotguns, etc.

Games have a lot of flexibility in what they do so a game engine also
needs to be flexible here.

Managing these weapons/items are done in various ways. There might be
an inventory screen or there might be a key to drop the active one.
Similarly, pickup might be automatic (simply walking over) or it might
require a manual interaction (usually, pressing `e`). This is another
key area where VR differs, since picking something up in a realistic,
simulated way is usually a key part of the gameplay experience in VR.
Not everything held (VR or not) can be stored in inventory. Usually,
the rest are manually throwable. There often is also a way to drop a
held weapon or weapon-like item from one's inventory to the floor (or
just trashing it).

#### First person shooter (FPS) controls

Some controls apply mainly to first person shooters, although similar
controls might still work for other kinds of games.

Guns can fire hitscan or projectiles in the direction where the
crosshair is facing, typically the center of the screen. Sometimes
bullets use "histcan" and other times they use projectile simulations.
Melee weapons typically work very similarly to guns, but with a very
short maximum reach and a complicated animation.

Accuracy is not perfect and this imperfection is represented as the
concept of "spread". Often, the crosshair will resize to show the
current weapon spread, giving an intuitive feel of the relative
accuracy when weapon switching.

Because of the way aiming works, the mouse must not visible unless a
menu is visible, or at least some other non-typical mode is in place,
such as the overhead observer mode or a "live" map editor that can be
edited while players are playing. As far as the controls are
concerned, any interaction where the mouse is visible is probably a
temporary "genre" shift into a completely different set of controls.
However, some games do allow you to continue firing while in menus if
continuously holding the left mouse button.

As mentioned before, `r` is typically used for reloading if there is
reloading in the game. Usually, pressing fire (the left mouse button)
while needing to reload automatically reloads, but this can be
disadvantageous since weapon switching can be faster than reloading,
so it is often configurable.

In single player, weapons might automatically reload over time when
not active, or might reload at level transitions. This removes some
micromanagement. Different weapons might have different ammo types,
and the ammo type might be allowed to go beyond the cap if the weapon
isn't fully reloaded, meaning that reloading isn't necessary for
optimal min/max of ammo pickup. Weapons typically have some finite
ammo cap.

In multiplayer, `tab` is often used for displaying scores/points. More
rarely, it can also be used to show a leaderboard in single player.

Other statistics, such as accuracy, kills, deaths, etc., can be
tracked and displayed with another key, often `alt`. The stats might
not be visible until after the end of a game, though.

### Overhead camera controls

Overhead camera controls are generally used in strategy games, such as
real-time or turn-based strategy. They are also used in some kinds of
role-playing games (RPGs); in particular, the RPGs that focus on
controlling parties rather than individual characters. Other kinds of
games might also use overhead controls for certain modes, such as an
observer mode or map editor. Some kinds of 2D games fake an overhead
3D perspective, especially isometric games. These controls are
considerably less standardized than the first person or even other
third person controls.

*More details will be added at some point.*
