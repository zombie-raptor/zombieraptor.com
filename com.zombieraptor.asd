(asdf:defsystem #:com.zombieraptor
  :description "The website generator for the Zombie Raptor game engine"
  :version "2021.2.4"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "N/A" ; TODO: license
  :homepage "https://gitlab.com/zombie-raptor/zombieraptor.com"
  :bug-tracker "https://gitlab.com/zombie-raptor/zombieraptor.com/issues"
  :source-control (:git "https://gitlab.com/zombie-raptor/zombieraptor.com.git")
  :depends-on (:cl-documents
               :uiop
               :zr-utils)
  :components ((:file "package")
               (:file "website")))
