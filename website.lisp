(in-package #:com.zombieraptor)

;;; TODO: When the Markdown->HTML is ready, move most of the body to
;;; separate Markdown files.

(defun style.css (stream)
  (with-css (stream)
    (:body :line-height "1.3")
    (:h1 :color "#ff5305")
    (:h2 :color "#ff8a56")
    (:h3 :color "#ff8a56")
    (:body :font-family "sans-serif"
           :font-size "12pt"
           :max-width "650px"
           :margin "40px auto"
           :color "#fff"
           :background-color "#000")
    (:pre :font-family "monospace"
          :font-size "10pt"
          :color "#fff"
          :background-color "#111"
          :max-height "600px"
          :overflow-x "scroll"
          :overflow-y "scroll")
    (:a :text-decoration "none"
        :color "#6a99ff")
    (:|a:visited| :color "#c1c")
    (:code :font-family "monospace")))

(defmacro with-site-template ((stream html-title body-title)
                              &body contents)
  `(with-html (,stream)
     (:html
      :lang "en"
      (:head
       (:title ,html-title)
       (:meta :name "viewport"
              :content "width=device-width, initial-scale=1")
       ;; A blank favicon instead of a missing one.
       (:link :href "data:," :rel "icon")
       (:link :rel "stylesheet"
              :type "text/css"
              :href "style.css"))
      (:body
       (:a :href "index.html" (:h1 "Zombie Raptor"))
       ,@(if body-title `((:h3 ,body-title)) nil)
       ,@contents))))

(defun common-lisp.html (stream)
  (with-site-template (stream
                       "Zombie Raptor : Why Common Lisp?"
                       "Why Common Lisp?")
    (:p (:a :href "https://common-lisp.net/"
            "Common Lisp")
        " is a rare programming language that aims at satisfying "
        "two seemingly contradictory goals: interactive "
        "development and compile-time programming.")
    (:p "As a "
        (:a :href "https://en.wikipedia.org/wiki/Lisp_(programming_language)"
            "Lisp")
        ", Common Lisp focuses on trivial "
        "metaprogramming via syntactic macros and other compile-time "
        "constructs. In fact, virtually everything that can be "
        "done is doable at compilation time.")
    (:p "At the same time, Common Lisp development is typically "
        "done in an interactive manner with a very powerful "
        "read-eval-print-loop (REPL) that allows almost "
        "everything to be redefined at runtime during "
        "the development process.")
    (:p "This combination of features makes Common Lisp very "
        "suitable for game engine development, even if it is "
        "an unusual choice. The ideal is to do both the game "
        "engine and the game scripting with one language that "
        "is able to meet both goals simultaneously.")
    (:h3 "Libraries")
    (:p "One of the disadvantages of using a niche language is "
        "the lack of popular libraries for common tasks. Although "
        "this is a game engine project, other libraries have to be "
        "written.")
    (:p "Most of this code has wound up in a utility library, "
        (:a :href "https://gitlab.com/zombie-raptor/zr-utils"
            "called zr-utils")
        ".")))

(defun index.html (stream)
  (with-site-template (stream "Zombie Raptor" nil)
    (:p (:strong "Zombie Raptor")
        " is a 3D game engine that's "
        (:a :href "https://gitlab.com/zombie-raptor/zombie-raptor/"
            "in active development on GitLab.com")
        ". It is "
        (:a :href "common-lisp.html"
            "written in the Common Lisp programming language")
        ".")
    (:p "The engine will focus on the FPS, RTS, and RPG genres.")
    (:p "The game engine is < 10% complete. Screenshots will be "
        "posted over time as the engine is more complete.")))

(defun write-website-file (function pathname)
  (with-open-file (file-stream
                   pathname
                   :direction :output
                   :if-exists :supersede)
    (funcall function file-stream)))

(defun main ()
  (let ((output-path (uiop:subpathname (asdf:system-source-directory :com.zombieraptor)
                                       #P"public/")))
    (uiop:ensure-all-directories-exist (list output-path))
    (let ((index.html (uiop:subpathname output-path "index.html"))
          (common-lisp.html (uiop:subpathname output-path "common-lisp.html"))
          (style.css (uiop:subpathname output-path "style.css")))
      (write-website-file #'index.html index.html)
      (write-website-file #'common-lisp.html common-lisp.html)
      (write-website-file #'style.css style.css))))
